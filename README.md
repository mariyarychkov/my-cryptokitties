# My CryptoKitties

A clone of famous DApp CryptoKitties (https://www.cryptokitties.co/), where each kitty is represented as a non-fungible token using the ERC-721 token standard on Ethereum. 